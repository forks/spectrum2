let
  pkgs = import <nixpkgs> {};
  libpurple = import ./libpurple.nix { pkgs = pkgs; };
  spectrum2 = import ./default.nix { pkgs = pkgs; libpurple = libpurple; };
  purple-discord = pkgs.purple-discord.overrideAttrs (origAttrs: rec {
    version = "unstable-2020-06-16";
    buildInputs = [ pkgs.json-glib libpurple pkgs.imagemagick ];
    src = pkgs.fetchFromGitHub {
      owner = "EionRobb";
      repo = "purple-discord";
      rev = "b0daa37f0ecf7bcfea60163ac3273dd7064ec133";
      sha256 = "0iqz8p6cfd7yvr31r7npcafz3azs1pf4j7dnw134v93dcvamz2hh";
    };
  });
  purple-slack = pkgs.purple-slack.overrideAttrs (origAttrs: rec {
    version = "unstable-2020-07-01";
    buildInputs = [ pkgs.json-glib libpurple ];
    src = pkgs.fetchFromGitHub {
      owner = "dylex";
      repo = "slack-libpurple";
      rev = "b61b4dd07987edde84ba7dcdef12746a32f37ebe";
      sha256 = "18klzdacdq1fn2bq3gw01l3jvfsi2ijvrbn187psfkwpylv7rv8j";
    };
  });
  purple-facebook = pkgs.purple-facebook.overrideAttrs (origAttrs: rec {
    pname = "purple-facebook";
    version = "unstable-2020-07-01";
    nativeBuildInputs = origAttrs.nativeBuildInputs ++ [ pkgs.pkg-config ];
    buildInputs = [ pkgs.json-glib libpurple ];
    src = pkgs.fetchFromGitHub {
      owner = "dequis";
      repo = "purple-facebook";
      rev = "106d7acac2618c4348512c4feb52f1f43d11f724";
      sha256 = "1xxhramzm135v4g8vjapy2gnc7hfwhmgr0ya8y520m1wh16q5csp";
    };
  });
in
pkgs.dockerTools.buildLayeredImage {
  name = "eu.gcr.io/etainfra/spectrum2";
  tag = "latest";
  contents = [ spectrum2 purple-discord purple-slack purple-facebook ];
  config.Entrypoint = [ "${spectrum2}/bin/spectrum2" ];
}
