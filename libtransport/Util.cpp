/**
 * libtransport -- C++ library for easy XMPP Transports development
 *
 * Copyright (C) 2011, Jan Kaluza <hanzz.k@gmail.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02111-1301 USA
 */

#include "transport/Util.h"

#include <algorithm>
#include <boost/algorithm/string.hpp>
#include <boost/filesystem.hpp>
#include <boost/foreach.hpp>
#include <boost/numeric/conversion/cast.hpp>
#include <iostream>
#include <iterator>

#include "transport/Config.h"

#include <grp.h>
#include <pwd.h>
#include <sys/resource.h>
#include <sys/stat.h>

#include "libgen.h"
#include "sys/signal.h"

using namespace boost::filesystem;

namespace Transport {

namespace Util {

int getRandomPort(const std::string &s) {
	unsigned long r = 0;
	BOOST_FOREACH (char c, s) { r += (int)c; }
	srand(time(NULL) + r);
	return 30000 + rand() % 10000;
}

std::string char2hex(char dec) {
	char dig1 = (dec & 0xF0) >> 4;
	char dig2 = (dec & 0x0F);
	if (0 <= dig1 && dig1 <= 9)
		dig1 += 48; // 0,48 in ascii
	if (10 <= dig1 && dig1 <= 15)
		dig1 += 65 - 10; // A,65 in ascii
	if (0 <= dig2 && dig2 <= 9)
		dig2 += 48;
	if (10 <= dig2 && dig2 <= 15)
		dig2 += 65 - 10;

	std::string r;
	r.append(&dig1, 1);
	r.append(&dig2, 1);
	return r;
}

std::string urlencode(const std::string &c) {
	std::string escaped;
	int max = c.length();
	for (int i = 0; i < max; i++) {
		if ((48 <= c[i] && c[i] <= 57) ||  // 0-9
		    (65 <= c[i] && c[i] <= 90) ||  // ABC...XYZ
		    (97 <= c[i] && c[i] <= 122) || // abc...xyz
		    (c[i] == '~' || c[i] == '-' || c[i] == '_' || c[i] == '.')) {
			escaped.append(&c[i], 1);
		} else {
			escaped.append("%");
			escaped.append(char2hex(c[i])); // converts char 255 to string "FF"
		}
	}
	return escaped;
}

} // namespace Util

} // namespace Transport
