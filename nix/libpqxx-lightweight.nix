{ pkgs ? import <nixpkgs> {} }:

pkgs.libpqxx.overrideAttrs (_: rec {
  buildInputs = [ pkgs.doxygen pkgs.xmlto pkgs.postgresql ];
  outputs = [ "out" "pkgconfig" ];
  postInstall = ''
      sed -i 's| -I${pkgs.postgresql}/include||' $out/lib/pkgconfig/libpqxx.pc
      mkdir -p $pkgconfig/lib/pkgconfig
      mv $out/lib/pkgconfig/libpqxx.pc $pkgconfig/libpqxx.pc
    '';
})
