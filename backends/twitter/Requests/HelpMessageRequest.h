#ifndef HELPMESSAGE_H
#define HELPMESSAGE_H

#include <boost/function.hpp>
#include <iostream>
#include <string>

#include "../libtwitcurl/twitcurl.h"
#include "transport/Logging.h"
#include "transport/ThreadPool.h"

using namespace Transport;

class HelpMessageRequest : public Thread {
	std::string user;
	std::string jid;
	std::string helpMsg;
	boost::function<void(std::string &, std::string &)> callBack;

  public:
	HelpMessageRequest(const std::string &_user, const std::string &jid,
	                   boost::function<void(std::string &, std::string &)> cb) {
		user = _user;
		callBack = cb;
		this->jid = jid;
	}

	void run();
	void finalize();
};

#endif
