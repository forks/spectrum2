/*
 * Implements XEP-0334: Message Processing Hints
 * Licensed under the Simplified BSD license.
 * See Documentation/Licenses/BSD-simplified.txt for more information.
 */

#include <Swiften/Serializer/PayloadSerializers/HintPayloadSerializer.h>
#include <Swiften/Serializer/XML/XMLElement.h>
#include <Swiften/Serializer/XML/XMLRawTextNode.h>
#include <Swiften/Serializer/XML/XMLTextNode.h>

#include <boost/shared_ptr.hpp>
#include <string>

namespace Swift {

HintPayloadSerializer::HintPayloadSerializer() : GenericPayloadSerializer<HintPayload>() {
}

std::string HintPayloadSerializer::serializePayload(std::shared_ptr<HintPayload> hint) const {
	std::string tagname = "";
	switch (hint->getType()) {
	case HintPayload::NoPermanentStore:
		tagname = "no-permanent-store";
		break;
	case HintPayload::NoStore:
		tagname = "no-store";
		break;
	case HintPayload::NoCopy:
		tagname = "no-copy";
		break;
	case HintPayload::Store:
		tagname = "store";
		break;
	}

	return XMLElement(tagname, "urn:xmpp:hints").serialize();
}

} // namespace Swift
