#include <boost/algorithm/string.hpp>
#include <boost/filesystem.hpp>
#include <boost/thread.hpp>

#include "Swiften/EventLoop/SimpleEventLoop.h"
#include "Swiften/Network/BoostNetworkFactories.h"
#include "frontends/xmpp/XMPPFrontendPlugin.h"
#include "transport/AdminInterface.h"
#include "transport/Config.h"
#include "transport/Frontend.h"
#include "transport/Logging.h"
#include "transport/MySQLBackend.h"
#include "transport/NetworkPluginServer.h"
#include "transport/PQXXBackend.h"
#include "transport/SQLite3Backend.h"
#include "transport/StorageBackend.h"
#include "transport/Transport.h"
#include "transport/UserManager.h"
#include "transport/UserRegistration.h"
#include "transport/UsersReconnecter.h"
#include "transport/Util.h"
#include <grp.h>
#include <pwd.h>
#include <sys/resource.h>

#include "libgen.h"
#include "sys/signal.h"
#include "sys/stat.h"
#include <malloc.h>
#include <sys/stat.h>

#include <boost/dll/runtime_symbol_info.hpp> // for program_location()
#include <boost/dll/shared_library.hpp>      // for shared_library

namespace dll = boost::dll;
using namespace Transport;
using namespace Transport::Util;

DEFINE_LOGGER(logger, "Spectrum");

Swift::SimpleEventLoop *eventLoop_ = NULL;
Component *component_ = NULL;
UserManager *userManager = NULL;
Config *config_ = NULL;

static void stop_spectrum() {
	userManager->removeAllUsers(false);
	component_->stop();
	eventLoop_->stop();
}

static void spectrum_sigint_handler(int sig) {
	eventLoop_->postEvent(&stop_spectrum);
}

static void spectrum_sigterm_handler(int sig) {
	eventLoop_->postEvent(&stop_spectrum);
}

static void _createDirectories(Transport::Config *config, boost::filesystem::path ph) {
	if (ph.empty() || exists(ph)) {
		return;
	}

	// First create branch, by calling ourself recursively
	_createDirectories(config, ph.branch_path());

	// Now that parent's path exists, create the directory
	boost::filesystem::create_directory(ph);
}

int mainloop() {
	mode_t old_cmask = umask(0007);

	Logging::initMainLogging(config_);

	Swift::SimpleEventLoop eventLoop;

	Swift::BoostNetworkFactories *factories = new Swift::BoostNetworkFactories(&eventLoop);

	Frontend *frontend = NULL;

	std::string frontend_name = CONFIG_STRING_DEFAULTED(config_, "service.frontend", "xmpp");
	std::string plugin_fc = "create_" + frontend_name + "_frontend_plugin";

	dll::shared_library self(dll::program_location());
	boost::function<std::shared_ptr<FrontendPlugin>()> creator;

	try {
		creator = self.get_alias<std::shared_ptr<FrontendPlugin>()>(plugin_fc);
	} catch (boost::system::system_error &e) {
		LOG4CXX_ERROR(logger, "Error when loading frontend " << e.what());
		return -3;
	}

	if (!creator) {
		LOG4CXX_ERROR(logger, "Unknown Frontend name " << frontend_name);
		return -3;
	}

	frontend = creator()->createFrontend();

	Component transport(frontend, &eventLoop, factories, config_, NULL);
	component_ = &transport;

	std::string error;
	StorageBackend *storageBackend = StorageBackend::createBackend(config_, error);
	if (storageBackend == NULL) {
		if (!error.empty()) {
			std::cerr << error << "\n";
			return -2;
		}
		std::cerr << "No storage backend configured.\n";
		return -3;
	} else if (!storageBackend->connect()) {
		std::cerr << "Can't connect to database. Check the log to find out the reason.\n";
		return -1;
	}

	Logging::redirect_stderr();

	userManager = frontend->createUserManager(&transport, storageBackend);
	UserRegistration *userRegistration = userManager->getUserRegistration();

	UsersReconnecter *usersReconnecter = new UsersReconnecter(&transport, storageBackend);

	NetworkPluginServer plugin(&transport, config_, userManager);
	plugin.start();

	AdminInterface adminInterface(&transport, userManager, &plugin, storageBackend, userRegistration);
	plugin.setAdminInterface(&adminInterface);
	transport.setAdminInterface(&adminInterface);

	eventLoop_ = &eventLoop;

	eventLoop.run();

	umask(old_cmask);

	delete userManager;
	delete usersReconnecter;
	delete storageBackend;
	delete factories;
	return 0;
}

int main(int argc, char **argv) {
	Config config(argc, argv);
	config_ = &config;
	boost::program_options::variables_map vm;
	std::string config_file;
	std::string jid;

	if (signal(SIGINT, spectrum_sigint_handler) == SIG_ERR) {
		std::cout << "SIGINT handler can't be set\n";
		return -1;
	}

	if (signal(SIGTERM, spectrum_sigterm_handler) == SIG_ERR) {
		std::cout << "SIGTERM handler can't be set\n";
		return -1;
	}
	boost::program_options::options_description desc(std::string("Spectrum version: ") + SPECTRUM_VERSION +
	                                                 "\nUsage: spectrum [OPTIONS] <config_file.cfg>\nAllowed options");
	desc.add_options()("help,h", "help")("no-debug,d", "Create coredumps on crash")(
	    "jid,j", boost::program_options::value<std::string>(&jid)->default_value(""),
	    "Specify JID of transport manually")(
	    "config", boost::program_options::value<std::string>(&config_file)->default_value(""),
	    "Config file")("version,v", "Shows Spectrum version");
	try {
		boost::program_options::positional_options_description p;
		p.add("config", -1);
		boost::program_options::store(boost::program_options::command_line_parser(argc, argv)
		                                  .options(desc)
		                                  .positional(p)
		                                  .allow_unregistered()
		                                  .run(),
		                              vm);
		boost::program_options::notify(vm);

		if (vm.count("version")) {
			std::cout << SPECTRUM_VERSION << "\n";
			return 0;
		}

		if (vm.count("help")) {
			std::cout << desc << "\n";
			return 1;
		}

		if (vm.count("config") == 0) {
			std::cout << desc << "\n";
			return 1;
		}
	} catch (...) {
		std::cout << desc << "\n";
		return 1;
	}

	if (!config.load(vm["config"].as<std::string>(), jid)) {
		std::cerr << "Can't load configuration file.\n";
		return 1;
	}

	mainloop();
}
