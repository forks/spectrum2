/*
 * Copyright (c) 2011 Jan Kaluza
 * Licensed under the Simplified BSD license.
 * See Documentation/Licenses/BSD-simplified.txt for more information.
 */

#pragma once

#include <Swiften/Elements/Payload.h>

#include <boost/shared_ptr.hpp>
#include <string>
#include <vector>

namespace Swift {
class AttentionPayload : public Payload {
  public:
	typedef std::shared_ptr<AttentionPayload> ref;

  public:
	AttentionPayload();
};
} // namespace Swift
