/*
 * Copyright (c) 2011 Jan Kaluza
 * Licensed under the Simplified BSD license.
 * See Documentation/Licenses/BSD-simplified.txt for more information.
 */

#include <Swiften/Serializer/PayloadSerializers/InvisibleSerializer.h>
#include <Swiften/Serializer/XML/XMLElement.h>
#include <Swiften/Serializer/XML/XMLRawTextNode.h>
#include <Swiften/Serializer/XML/XMLTextNode.h>

#include <boost/shared_ptr.hpp>

namespace Swift {

// This payload is NOT part of ANY XEP and it is only
// libtransport related extension.
InvisibleSerializer::InvisibleSerializer() : GenericPayloadSerializer<InvisiblePayload>() {
}

std::string InvisibleSerializer::serializePayload(std::shared_ptr<InvisiblePayload> attention) const {
	XMLElement attentionElement("invisible", "urn:xmpp:invisible:0");

	return attentionElement.serialize();
}

} // namespace Swift
