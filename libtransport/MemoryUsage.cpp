/**
 * XMPP - libpurple transport
 *
 * Copyright (C) 2009, Jan Kaluza <hanzz@soc.pidgin.im>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02111-1301 USA
 */

#include "transport/MemoryUsage.h"

#include <algorithm>
#include <boost/lexical_cast.hpp>
#include <cstring>
#include <fstream>
#include <iostream>
#include <sstream>
#include <sys/param.h>

namespace Transport {

void process_mem_usage(double &shared, double &resident_set, pid_t pid) {
	using std::ifstream;
	using std::ios_base;
	using std::string;

	shared = 0.0;
	resident_set = 0.0;

	// 'file' stat seems to give the most reliable results
	std::string f = "/proc/self/statm";
	if (pid != 0) {
		f = "/proc/" + boost::lexical_cast<std::string>(pid) + "/statm";
	}
	ifstream stat_stream(f.c_str(), ios_base::in);
	if (!stat_stream.is_open()) {
		shared = 0;
		resident_set = 0;
		return;
	}

	// dummy vars for leading entries in stat that we don't care about
	//
	string pid1, comm, state, ppid, pgrp, session, tty_nr;
	string tpgid, flags, minflt, cminflt, majflt, cmajflt;
	string utime, stime, cutime, cstime, priority, nice;
	string O, itrealvalue, starttime;

	// the two fields we want
	//
	unsigned long shared_;
	long rss;

	stat_stream >> O >> rss >> shared_; // don't care about the rest

	long page_size_kb = sysconf(_SC_PAGE_SIZE) / 1024; // in case x86-64 is configured to use 2MB pages
	shared = shared_ * page_size_kb;
	resident_set = rss * page_size_kb;
}

} // namespace Transport
