/**
 * libtransport -- C++ library for easy XMPP Transports development
 *
 * Copyright (C) 2011, Jan Kaluza <hanzz.k@gmail.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02111-1301 USA
 */

#include "XMPPUser.h"

#include <stdio.h>
#include <stdlib.h>

#include <boost/foreach.hpp>

#include "Swiften/Elements/SpectrumErrorPayload.h"
#include "Swiften/Elements/StreamError.h"
#include "Swiften/Queries/IQRouter.h"
#include "XMPPFrontend.h"
#include "transport/Logging.h"
#include "transport/Transport.h"
#include "transport/UserManager.h"

#define foreach BOOST_FOREACH

namespace Transport {

DEFINE_LOGGER(logger, "XMPPUser");

XMPPUser::XMPPUser(const Swift::JID &jid, UserInfo &userInfo, Component *component, UserManager *userManager)
    : User(jid, userInfo, component, userManager) {
	m_jid = jid.toBare();

	m_component = component;
	m_userManager = userManager;
	m_userInfo = userInfo;
	m_rooms = std::shared_ptr<Swift::DiscoItems>(new Swift::DiscoItems());
}

XMPPUser::~XMPPUser() {
	if (m_vcardRequests.size() != 0) {
		LOG4CXX_INFO(logger, m_jid.toString() << ": Removing " << m_vcardRequests.size() << " unresponded IQs");
		BOOST_FOREACH (Swift::GetVCardRequest::ref request, m_vcardRequests) {
			request->onResponse.disconnect_all_slots();
			static_cast<XMPPFrontend *>(m_component->getFrontend())->getIQRouter()->removeHandler(request);
		}
		m_vcardRequests.clear();
	}
}

void XMPPUser::disconnectUser(const std::string &error, Swift::SpectrumErrorPayload::Error e) {
}

void XMPPUser::handleVCardReceived(std::shared_ptr<Swift::VCard> vcard, Swift::ErrorPayload::ref error,
                                   Swift::GetVCardRequest::ref request) {
	m_vcardRequests.remove(request);
	request->onResponse.disconnect_all_slots();
	m_component->getFrontend()->onVCardUpdated(this, vcard);
}

void XMPPUser::requestVCard() {
	LOG4CXX_INFO(logger, m_jid.toString() << ": Requesting VCard");

	Swift::GetVCardRequest::ref request =
	    Swift::GetVCardRequest::create(m_jid, static_cast<XMPPFrontend *>(m_component->getFrontend())->getIQRouter());
	request->onResponse.connect(boost::bind(&XMPPUser::handleVCardReceived, this, _1, _2, request));
	request->send();
	m_vcardRequests.push_back(request);
}

void XMPPUser::clearRoomList() {
	m_rooms = std::shared_ptr<Swift::DiscoItems>(new Swift::DiscoItems());
}

void XMPPUser::addRoomToRoomList(const std::string &handle, const std::string &name) {
	LOG4CXX_INFO(logger, m_jid.toString() << ": Adding room " << handle << " named " << name);
	m_userManager->m_storageBackend->addMucName(getUserId(), handle, name);
	m_rooms->addItem(Swift::DiscoItems::Item(name, handle));
}

void XMPPUser::handleRoomInformation(const std::string &room, const std::string &name, const std::string *subject) {
	LOG4CXX_INFO(logger, m_jid.toString() << ": Room information received for " << room << ": name " << name);
	struct RoomInformation room_info = {.room = room, .name = name};
	m_room_info[room] = room_info;
	m_userManager->m_storageBackend->beginTransaction();
	if (m_userManager->m_storageBackend->getMucName(getUserId(), room) == "") {
		m_userManager->m_storageBackend->addMucName(getUserId(), room, name);
	}
	m_userManager->m_storageBackend->commitTransaction();
}

} // namespace Transport
