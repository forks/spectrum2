{ pkgs ? import <nixpkgs> {}, ... }:

pkgs.pidgin.overrideAttrs (origAttrs: rec {
  pname = "libpurple";
  buildInputs = origAttrs.buildInputs ++ origAttrs.propagatedBuildInputs;
  configureFlags = origAttrs.configureFlags ++ [
    "--disable-avahi"
    "--disable-gstreamer"
    "--disable-meanwhile"
    "--disable-gnutls"
    "--disable-cyrus-sasl"
    "--disable-farstream"
    "--disable-vv"
    "--disable-dbus"
    "--disable-perl"
    "--disable-schemas-install"
  ];
  propagatedBuildInputs = [];
  preInstall = ''cd libpurple'';
  postInstall = ''rm -rf $out/bin'';
})
