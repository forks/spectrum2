{ pkgs ? import <nixpkgs> {}, libpurple ? import ./libpurple.nix {}, pqxx ? import ./libpqxx-lightweight.nix {}, swiften ? import ./swiften-lightweight.nix {}, ... }:

let
  gitignoreSrc = pkgs.fetchFromGitHub {
    owner = "hercules-ci";
    repo = "gitignore";
    rev = "647d0821b590ee96056f4593640534542d8700e5";
    sha256 = "sha256:0ks37vclz2jww9q0fvkk9jyhscw0ial8yx2fpakra994dm12yy1d";
  };
  inherit (import gitignoreSrc { inherit (pkgs) lib; }) gitignoreFilter;
  filter = gitignoreFilter ../.;
in
with pkgs;
stdenv.mkDerivation {
  name = "spectrum2";
  src = builtins.filterSource
    (path: type: !(lib.strings.hasSuffix (baseNameOf path) "nix") && filter path type)
    ../.;
  nativeBuildInputs = [ cmake pkg-config ];
  buildInputs = [ swiften pqxx pqxx.pkgconfig postgresql.lib libpurple libev log4cxx glib pcre boost protobuf curl jsoncpp ];
}
