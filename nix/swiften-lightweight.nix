{ pkgs ? import <nixpkgs> {} }:

let
  pkgs-eta = import (builtins.fetchTarball {
    # Descriptive name to make the store path easier to identify
    name = "nixos-with-fixed-swiften";
    url = "https://github.com/nixos/nixpkgs/archive/557644a1258902d20619679755dfa393b4fdad07.tar.gz";
    sha256 = "0ia6c5c1zxmslmdahdrg745jb1vk41l0cl7y0h2xmzf5ls80rx3z";
  }) {};
in
pkgs-eta.swiften.overrideAttrs (orig: rec {
  outputs = ["out" "dev"];
  buildInputs = orig.buildInputs ++ [ pkgs-eta.boost.dev ];
  propagatedBuildInputs = [ pkgs-eta.openssl pkgs-eta.boost.out ];
  outputBin = "dev";
  postFixup = ''
    mv -v $out/bin $dev/
  '';
})
